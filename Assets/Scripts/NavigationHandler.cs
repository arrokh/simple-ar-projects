﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationHandler : MonoBehaviour
{

    public void NavigateToScene(int index)
    {
        SceneManager.LoadScene(index);
    }
}
