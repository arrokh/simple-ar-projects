﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToOpenPageHandler : MonoBehaviour
{
    enum LinkOptions
    {
        UNKNOWN,
        MARKER_1,
        MARKER_2,
        MARKER_3,
        MARKER_4,
    }

    [SerializeField]
    private LinkOptions link = LinkOptions.UNKNOWN;

    private void OnMouseDown()
    {
        string url = "";

        switch (link)
        {
            case LinkOptions.MARKER_1:
                url = "https://www.youtube.com/watch?v=MbjZtz4NWik";
                break;
            case LinkOptions.MARKER_2:
                url = "https://www.youtube.com/watch?v=F4xmSPHDDVI&t=10s";
                break;
            case LinkOptions.MARKER_3:
                url = "https://www.youtube.com/watch?v=hWOdptwTguY";
                break;
            case LinkOptions.MARKER_4:
                url = "https://www.youtube.com/watch?v=FmuyQw3m2OU";
                break;

            case LinkOptions.UNKNOWN:
            default:
                break;
        }

        if (url.Length > 0)
        Application.OpenURL(url);
    }
}
