﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigateAfterDelay : MonoBehaviour
{
    [SerializeField]
    private float delay = 2.0f;

    [SerializeField]
    private int indexTargetScene = 1;

    void Start()
    {
        StartCoroutine(NavigateToSceneAfterDelay());
    }

    private IEnumerator NavigateToSceneAfterDelay()
    {
        yield return new WaitForSeconds(delay);

        SceneManager.LoadScene(indexTargetScene);
    }
}
